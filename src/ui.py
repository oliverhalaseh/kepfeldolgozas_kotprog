import PyQt5.QtWidgets as widgets
from PyQt5.uic import loadUi
from PyQt5.QtGui import QIcon, QPixmap, QImage
from os import getcwd
import cv2
import numpy as np
from math import floor


class TextExtractor(widgets.QMainWindow):
    """Dialog window for TextExtractor application."""

    DEFAULT_COLOR = (168, 164, 50)

    def __init__(self):
        super(TextExtractor, self).__init__()

        self.filename = ''
        self.image = None
        self.points = []
        self.image_preview = None
        self.grey_image = None

        # load UI
        loadUi('./src/mainwindow.ui', self)
        self.setWindowTitle('TextExtractor')

        self.initHandlers()

    def initHandlers(self):
        self.browseButton.clicked.connect(self.onOpenFileDialog)
        self.buttonBox.button(
            widgets.QDialogButtonBox.Ok).clicked.connect(self.onOpenImage)
        self.buttonBox.button(widgets.QDialogButtonBox.Reset).clicked.connect(
            self.onResetValues)
        self.cropImageButton.clicked.connect(self.onCropImage)
        self.cropImageCheckBox.stateChanged.connect(self.onCropToggled)
        self.threshHorizontalSlider.valueChanged.connect(self.onThreshChanged)
        self.fileLineEdit.setText(
            'C:/Users/hulye/Pictures/test_images/IMG_20211003_110607.jpg')
        self.filename = 'C:/Users/hulye/Pictures/test_images/IMG_20211003_110607.jpg'

    def onOpenFileDialog(self):
        """Open FileDialog window. And set self.filename with the selected value."""
        self.filename = widgets.QFileDialog.getOpenFileName(
            self, 'Select an image file.', getcwd(), 'Images (*.png, *.jpg)')[0]
        self.fileLineEdit.setText(self.filename)
        self.points = []

    def onOpenImage(self):
        """ Opens Image in a cv2 window. """
        if self.filename == '':
            widgets.QMessageBox.warning(
                self, 'Warning', 'No image selected.', buttons=widgets.QMessageBox.Ok)
            return
        self.image_preview = cv2.imread(self.filename, cv2.IMREAD_COLOR)
        max_image_size = 1080
        #self.image_preview = cv2.resize(self.image_preview, self.calcWidthAndHeight(max_image_size))
        cv2.namedWindow('Image', cv2.WINDOW_NORMAL)
        #cv2.resizeWindow('Image', self.calcWidthAndHeight(1000))
        cv2.setMouseCallback('Image', self.onImageClick)
        cv2.imshow('Image', self.image_preview)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        """image = self.image_cv2qt(img=image, new_width=self.imageLabel.width())
        pixmap = QPixmap(image)
        self.imageLabel.setPixmap(pixmap)"""

    def onResetValues(self):
        self.fileLineEdit.clear()
        self.filename = ''
        cv2.destroyAllWindows()

    def onCropToggled(self):
        self.cropImageButton.setEnabled(self.cropImageCheckBox.isChecked())

    def onCropImage(self):
        flattend_image = self.transform_image_by_points()
        self.grey_image = cv2.cvtColor(flattend_image, cv2.COLOR_RGB2GRAY)
        #th, self.image_preview = cv2.threshold(self.grey_image, 0,255,  cv2.THRESH_OTSU|cv2.THRESH_BINARY_INV)
        self.image_preview = cv2.adaptiveThreshold(
            self.grey_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, -1)
        cv2.imshow('Image', self.image_preview)

    def onThreshChanged(self, value):
        print(value)
        #th, self.image_preview = cv2.threshold(self.grey_image,value,255, cv2.THRESH_OTSU|cv2.THRESH_BINARY_INV)

        self.image_preview = cv2.adaptiveThreshold(
            self.grey_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, value, -1)
        cv2.imshow('Image', self.image_preview)
        print('asd')

    def onImageClick(self, event, x, y, flags, params):
        """ Register a click listener for the cv2 image window. """
        if event == cv2.EVENT_LBUTTONDOWN and len(self.points) < 4:
            self.points.append((x, y))
            cv2.circle(self.image_preview,
                       self.points[-1], 4, TextExtractor.DEFAULT_COLOR, 12)
            if len(self.points) >= 2:
                cv2.line(
                    self.image_preview, self.points[-2], self.points[-1], TextExtractor.DEFAULT_COLOR, 6)
            if len(self.points) == 4:
                cv2.line(
                    self.image_preview, self.points[-1], self.points[0], TextExtractor.DEFAULT_COLOR, 6)
            cv2.imshow('Image', self.image_preview)

    def calcWidthAndHeight(self, long_side_size):
        height, width, color = self.image_preview.shape
        if height > width:
            width = floor(width * (long_side_size/height))
            height = long_side_size
        else:
            height = floor(height * (long_side_size/width))
            width = long_side_size
        return (width, height)

    def image_cv2qt(self, img, new_width):
        imgrgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        height, width, channel = imgrgb.shape
        new_height = height * (new_width/width)
        dsize = (floor(new_width), floor(new_height))
        resized_imagergb = cv2.resize(
            imgrgb, dsize, interpolation=cv2.INTER_AREA)
        bytesPerLine = 3 * new_width
        qImg = QImage(resized_imagergb.data,
                      dsize[0], dsize[1], bytesPerLine, QImage.Format_RGB888)
        return qImg

    def transform_image_by_points(self):
        pts = np.array(self.points, dtype='float32')

        def order_points(pts):
            # initialzie a list of coordinates that will be ordered
            # such that the first entry in the list is the top-left,
            # the second entry is the top-right, the third is the
            # bottom-right, and the fourth is the bottom-left
            rect = np.zeros((4, 2), dtype="float32")
            # the top-left point will have the smallest sum, whereas
            # the bottom-right point will have the largest sum
            print(pts)
            s = pts.sum(axis=1)
            rect[0] = pts[np.argmin(s)]
            rect[2] = pts[np.argmax(s)]
            # now, compute the difference between the points, the
            # top-right point will have the smallest difference,
            # whereas the bottom-left will have the largest difference
            diff = np.diff(pts, axis=1)
            rect[1] = pts[np.argmin(diff)]
            rect[3] = pts[np.argmax(diff)]
            # return the ordered coordinates
            return rect

        # obtain a consistent order of the points and unpack them
        # individually
        rect = order_points(pts)
        (tl, tr, br, bl) = rect
        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")
        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(
            self.image_preview, M, (maxWidth, maxHeight))
        # return the warped image
        return warped
