from ui import TextExtractor
from PyQt5.QtWidgets import  QApplication
from sys import argv


def main():
    app = QApplication(argv)

    textExtractor = TextExtractor()
    textExtractor.show()

    app.exec()

if __name__ == '__main__':
    main()